package com.reche.reche;

import java.util.List;

/**
 * Created by nitinkumar on 08/03/18.
 */

public class Event {
    private List<String> tags;
    private String authorId;
    private String authorName;
    private String title;
    private String description;
    private List<String> reportedBy;
    private String type;
    private String time;
    private String incentiveType;
    private int incentiveValue;
    private String id;
    private Boolean isBookmarked;

    public Event(){}

    public Boolean getBookmarked() {
        return isBookmarked;
    }

    public void setBookmarked(Boolean bookmarked) {
        isBookmarked = bookmarked;
    }

    public Event(String id, List<String> tags, String authorId, String authorName, String title, String description, List<String> reportedBy, String type, String time, String incentiveType, int incentiveValue) {
        this.tags = tags;
        this.authorId = authorId;
        this.authorName = authorName;
        this.title = title;
        this.description = description;
        this.reportedBy = reportedBy;
        this.type = type;
        this.time = time;
        this.incentiveType = incentiveType;

        this.incentiveValue = incentiveValue;
        this.id = id;
        this.setBookmarked(false);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(List<String> reportedBy) {
        this.reportedBy = reportedBy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIncentiveType() {
        return incentiveType;
    }

    public void setIncentiveType(String incentiveType) {
        this.incentiveType = incentiveType;
    }

    public int getIncentiveValue() {
        return incentiveValue;
    }

    public void setIncentiveValue(int incentiveValue) {
        this.incentiveValue = incentiveValue;
    }
}
