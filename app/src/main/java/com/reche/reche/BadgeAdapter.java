package com.reche.reche;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Mallock on 3/27/2018.
 */

public class BadgeAdapter extends Adapter<BadgeAdapter.ViewHolder> {

    public BadgeAdapter(ArrayList<Badge> badges, Activity activity) {
        this.badges = badges;
        this.activity = activity;
    }

    ArrayList<Badge> badges;
    Activity activity;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.badge_list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.imageView.setImageResource(activity.getResources().getIdentifier(badges.get(position).getBadge_file_name(), "drawable", activity.getPackageName()));
        holder.textView.setText(badges.get(position).getBadge_name());
    }

    @Override
    public int getItemCount() {
        return badges.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.badge_image);
            textView = itemView.findViewById(R.id.badge_text);
        }
    }

}
