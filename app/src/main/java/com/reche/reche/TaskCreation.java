package com.reche.reche;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TaskCreation extends AppCompatActivity {



    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    List<String> ongoingTaskList = new ArrayList<>();

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_updated);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.lightBlueB));

        dateView = (TextView) findViewById(R.id.end_date);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);

    }

    public void setDate(View view) {
        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }



    public void submitTaskFunc(View view) {

        EditText description = (EditText) findViewById(R.id.description_text);

        EditText title = (EditText) findViewById(R.id.title_text);

        Spinner taskTypeSpinner = (Spinner)findViewById(R.id.task_type_spinner);

        EditText durationTextEdit = (EditText) findViewById(R.id.duration_text);
        Spinner duration_type = (Spinner)findViewById(R.id.duration_type);

        EditText incentiveValueText = (EditText) findViewById(R.id.amount_text);
        Spinner incentiveType = (Spinner)findViewById(R.id.incentive_type);

        Intent intent = getIntent();

        String userId = intent.getStringExtra("authorID");
        String userName = intent.getStringExtra("user_name");

        String titleName = title.getText().toString();

        String descriptionText = description.getText().toString();

        String taskText = taskTypeSpinner.getSelectedItem().toString();


        String incentiveTypeText = incentiveType.getSelectedItem().toString();
        String incentiveValueTextValue = incentiveValueText.getText().toString();

        String durationText = durationTextEdit.getText().toString();
        String durationType = duration_type.getSelectedItem().toString();

        List<String> tags = new ArrayList<>();
        List<String> reportedBy = new ArrayList<>();
        tags.add("#AI");
        tags.add("#ImageAnalysis");
        reportedBy.add("a1");
        reportedBy.add("a2");

        DatabaseReference eventsPushRef = ref.child("events").push();
        final DatabaseReference ongoingTaskRef = ref.child("profiles/" + userId + "/ongoingTasks");

        final String eventId = eventsPushRef.getKey();

        eventsPushRef.setValue(new Event(eventId, tags, userId, userName, titleName,
                descriptionText, reportedBy, taskText, durationText + " Hr", incentiveTypeText, Integer.parseInt(incentiveValueTextValue)));

        ongoingTaskRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ongoingTaskList = (List<String>) dataSnapshot.getValue();
                if (ongoingTaskList == null) {
                    Log.d("FB", "Null ongoingTaskList");
                    ongoingTaskList = new ArrayList<String>();
                }
                ongoingTaskList.add(eventId);
                ongoingTaskRef.setValue(ongoingTaskList);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        Intent newintent = new Intent(this, FeedActivity.class);
//        startActivity(newintent);
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
