package com.reche.reche;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * known bugs
 * images won't set
 * tags won't get updated because of latency from firebase
 */
public class TaskDetailActivity extends AppCompatActivity {

    String TAG = this.getClass().getSimpleName();
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference("events/" + StaticClass.akshatString);
    String eventId = StaticClass.akshatString;
    int MAX_CHARACTERS = 10;

    List<String> tags;

    Event currentEvent;

    TextView authorName;
    TextView authorRating;
    TextView title;
    TextView description;
    ImageView typeImage;
    TextView typeText;
    ImageView incentiveImage;
    TextView incentiveText;
    TextView timeText;

    Button participateButton;

    DatabaseReference participatingTasksRef;

    //List<String> ongoingTaskList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        participatingTasksRef = database.getReference().child("profiles/"+ FeedActivity.localUserId + "/participatingTasks");
        Log.e("static context", "profiles/"+ FeedActivity.localUserId + "/participatingTasks");

        //Toast.makeText(this, StaticClass.akshatString, Toast.LENGTH_SHORT).show();

        authorName = findViewById(R.id.researcher_name_text);
        authorRating = findViewById(R.id.researcher_rating);
        title = findViewById(R.id.task_name);
        description = findViewById(R.id.task_description);
        typeImage = findViewById(R.id.ic_survey);
        typeText = findViewById(R.id.text_survey);
        incentiveImage = findViewById(R.id.ic_amazon);
        incentiveText = findViewById(R.id.text_amazon);
        timeText = findViewById(R.id.text_time);
        participateButton = (Button)findViewById(R.id.participate_button);
        if (FeedActivity.participatingTasks.contains(eventId)) {
            //participateButton.setBackgroundColor(Color.TRANSPARENT);

//            participateButton.setEnabled(false);
            participateButton.setAlpha(.5f);
            participateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(TaskDetailActivity.this, "Already Participating!", Toast.LENGTH_LONG).show();
                }
            });

            //participateButton.setClickable(false);

        }

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event = dataSnapshot.getValue(Event.class);
                assert event != null;
                StaticClass.nitinString = event.getAuthorId();
                populateEvent(event);
                currentEvent = event;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        String tags_list[] = {"DHCS", "People", "Research", "FGD", "Reche"};
        tags = new ArrayList<>();
        tags.addAll(Arrays.asList(tags_list));

//        Collections.sort(tags, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.length() - o2.length();
//            }
//        });

        Log.e(TAG, String.valueOf(tags));
        final LinearLayout tagsParentLayout = findViewById(R.id.tags_parent_layout);

        int remaining = MAX_CHARACTERS;
        ArrayList<LinearLayout> linearLayouts = new ArrayList<>();
        LinearLayout currentLayout = new LinearLayout(this);
        currentLayout.setOrientation(LinearLayout.HORIZONTAL);

        int count = 0;
        for (String tag : tags) {
            Button button = new Button(this);
            button.setText("#" + tag);
            button.setBackgroundResource(R.layout.oval_button);
            button.setPadding(5, 2, 5, 2);
            LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, tag.length());
            buttonLayoutParams.setMargins(50, 10, 0, 0);
            button.setLayoutParams(buttonLayoutParams);
            int buttonWidth = tag.length();
            if (buttonWidth <= remaining) {
                currentLayout.addView(button);
                remaining -= buttonWidth;
                Log.e(TAG, "added " + tag + " to " + count);
            } else {
                linearLayouts.add(currentLayout);
                count++;
                currentLayout = new LinearLayout(this);
                currentLayout.setOrientation(LinearLayout.HORIZONTAL);
                currentLayout.addView(button);
                remaining = MAX_CHARACTERS - buttonWidth;
                Log.e(TAG, "added " + tag + " to " + count);
            }
        }
        linearLayouts.add(currentLayout);
        for (LinearLayout linearLayout : linearLayouts) {
            tagsParentLayout.addView(linearLayout);
        }


        View researcherIcon = findViewById(R.id.researcher_icon);
        researcherIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TaskDetailActivity.this, ResearcherProfileActivity.class));
            }
        });
    }

    private void populateEvent(Event event) {
        authorName.setText(event.getAuthorName());
        authorRating.setText("3.3");
        title.setText(event.getTitle());
        description.setText(event.getDescription());
//        typeImage.setImageResource();
        typeText.setText(event.getType());
        incentiveText.setText(String.valueOf(event.getIncentiveValue()));
        timeText.setText(event.getTime());

        if (event.getIncentiveType().equals("Amazon")) {
            Picasso.get().load(R.drawable.amazon).into(incentiveImage);
        }
        if (event.getIncentiveType().equals("Paytm")) {
            Picasso.get().load(R.drawable.paytm).into(incentiveImage);
        }
        if (event.getIncentiveType().equals("Cash")) {
            Picasso.get().load(R.drawable.rupee).into(incentiveImage);
        }
        if (event.getType().equals("FGD")) {
            Picasso.get().load(R.drawable.fgd).into(typeImage);
        }
        if (event.getType().equals("Survey")) {
            Picasso.get().load(R.drawable.survey).into(typeImage);
        }
        if (event.getType().equals("Interview")) {
            Picasso.get().load(R.drawable.interview).into(typeImage);
        }

        tags = event.getTags();
    }

    public void participateClicked(View view) {
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(this)
                        .setTitle(currentEvent.getTitle())
                        .setMessage("Are you sure you want to participate?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(TaskDetailActivity.this, "Participation confirmed!", Toast.LENGTH_LONG).show();
                                FeedActivity.participatingTasks.add(eventId);
                                Log.d("static context", FeedActivity.participatingTasks.toString());
                                participatingTasksRef.setValue(FeedActivity.participatingTasks);
                                finish();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .setCancelable(true);

// Show the AlertDialog.
        AlertDialog alertDialog = alertDialogBuilder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
