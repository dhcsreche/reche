package com.reche.reche;

import java.util.List;

/**
 * Created by nitinkumar on 28/03/18.
 */

public class Profile {
    String email;
    String name;
    String affiliation;
    double rating;
    Boolean verified;
    List<String> badges;
    String bio;
    List<String> interests;
    Boolean isResearcher;
    List<String> bookmarks;
    List<String> ongoingTasks;
    int money;
    List<String> participatingTasks;

    public List<String> getParticipatingTasks() {
        return participatingTasks;
    }

    public void setParticipatingTasks(List<String> participatingTasks) {
        this.participatingTasks = participatingTasks;
    }

    public List<String> getOngoingTasks() {
        return ongoingTasks;
    }

    public void setOngoingTasks(List<String> ongoingTasks) {
        this.ongoingTasks = ongoingTasks;
    }

    public Profile(String email, String name, String affiliation, double rating, Boolean verified, List<String> badges, String bio, List<String> interests, Boolean isResearcher, List<String> bookmarks, List<String> ongoingTasks, int money, List<String> participatingTasks) {
        this.email = email;
        this.name = name;
        this.affiliation = affiliation;
        this.rating = rating;
        this.verified = verified;
        this.badges = badges;
        this.bio = bio;
        this.interests = interests;
        this.isResearcher = isResearcher;
        this.bookmarks = bookmarks;
        this.ongoingTasks = ongoingTasks;
        this.money = money;
        this.participatingTasks = participatingTasks;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Profile(){};

    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public List<String> getBadges() {
        return badges;
    }

    public void setBadges(List<String> badges) {
        this.badges = badges;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    public Boolean getResearcher() {
        return isResearcher;
    }

    public void setResearcher(Boolean researcher) {
        isResearcher = researcher;
    }

    public List<String> getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(List<String> bookmarks) {
        this.bookmarks = bookmarks;
    }
}
