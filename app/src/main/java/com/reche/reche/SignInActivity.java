package com.reche.reche;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    protected static GoogleSignInClient mGoogleSignInClient;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    DatabaseReference allProfiles;
    String hardcodedString = "-L9xXG8ArnM58W2B9Rhb";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        allProfiles = ref.child("profiles");
        //userProfileRef = ref.child("profiles/"+ getIntent().getStringExtra("authorID"));
        TextView skipSignIn = findViewById(R.id.signin_skip);
        skipSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, FeedActivity.class);
                intent.putExtra("user_name", "Nitin Kumar");
                intent.putExtra("userID", "PersonId");
                intent.putExtra("user_email", "nitin14069@iiitd.ac.in");
                intent.putExtra("authorID", hardcodedString);
                StaticClass.userName = "Nitin Kumar";
                StaticClass.userEmailId = "nitin14069@iiitd.ac.in";
                StaticClass.userProfilePic = null;
                StaticClass.userIdReche = hardcodedString;
                //if(personPhoto !=  null) intent.putExtra("profile_pic", personPhoto.toString());
                startActivity(intent);
            }
        });

        getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimary));

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    public void signInFunc(View view) {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void updateUI(GoogleSignInAccount account) {
        if(account != null) {
            //Fetching info, on successful sign in
            final String personName = account.getDisplayName();
            // String personGivenName = account.getGivenName();
            // String personFamilyName = account.getFamilyName();
            final String personEmail = account.getEmail();
            final String personId = account.getId();
            final Uri personPhoto = account.getPhotoUrl();


            final ArrayList<String> registeredProfiles = new ArrayList<>();
            final ArrayList<String> registeredAuthorId = new ArrayList<>();
            allProfiles.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, String.valueOf(dataSnapshot));
                    for (DataSnapshot eventItem : dataSnapshot.getChildren()) {
                        Profile newProfile = eventItem.getValue(Profile.class);
                        registeredProfiles.add(newProfile.getEmail());
                        registeredAuthorId.add(eventItem.getKey());
                        Log.d("Fetching Profiles", newProfile.email + " " + eventItem.getKey());
                    }
                    if (registeredProfiles.contains(personEmail)) {
                        Log.d("SignInActivity", "Already registered");
                        String authorId = registeredAuthorId.get(registeredProfiles.indexOf(personEmail));
                        Intent intent = new Intent(SignInActivity.this, FeedActivity.class);
                        intent.putExtra("user_name", personName);
                        intent.putExtra("userID", personId);
                        intent.putExtra("user_email", personEmail);
                        intent.putExtra("authorID", authorId);
                        if(personPhoto !=  null) intent.putExtra("profile_pic", personPhoto.toString());
                        StaticClass.userName = personName;
                        StaticClass.userEmailId = personEmail;
                        if(personPhoto !=  null) StaticClass.userProfilePic = personPhoto.toString();
                        StaticClass.userIdReche = authorId;
                        startActivity(intent);
                    }
                    else {
                        Log.d("SignInActivity", "Not registered");
                        Intent intent = new Intent(SignInActivity.this, SignOutActivity.class);
                        intent.putExtra("user_name", personName);
                        intent.putExtra("userID", personId);
                        intent.putExtra("user_email", personEmail);
                        if(personPhoto !=  null) intent.putExtra("profile_pic", personPhoto.toString());
                        StaticClass.userName = personName;
                        StaticClass.userEmailId = personEmail;
                        if(personPhoto !=  null) StaticClass.userProfilePic = personPhoto.toString();
                        startActivity(intent);
                    }
                    //Toast.makeText(SignInActivity.this, "DIFFERENT", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //Toast.makeText(SignInActivity.this, "TEXT", Toast.LENGTH_SHORT).show();


        } else {
            findViewById(R.id.signInButton).setVisibility(View.VISIBLE);
        }
    }

    private void openFeedPage(GoogleSignInAccount account) {
        if(account != null) {
            Intent intent = new Intent(this, FeedActivity.class);
            startActivity(intent);
            finish();
        } else {
            findViewById(R.id.signInButton).setVisibility(View.VISIBLE);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }
}
