package com.reche.reche;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;

public class ResearcherProfileActivity extends AppCompatActivity {


    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference("profiles/" + StaticClass.nitinString);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_researcher_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Profile profile = dataSnapshot.getValue(Profile.class);
                ((TextView) findViewById(R.id.researcher_name_text)).setText(profile.getName());
                ((TextView) findViewById(R.id.researcher_affiliation)).setText(profile.getAffiliation());
                ((TextView) findViewById(R.id.bio)).setText(profile.getBio());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        BadgeAdapter adapter = new BadgeAdapter(getBadges(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        DotsIndicator dotsIndicator = (DotsIndicator) findViewById(R.id.dots_indicator);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(viewPagerAdapter);
        dotsIndicator.setViewPager(viewPager);
    }

    private ArrayList<Badge> getBadges() {
        ArrayList<Badge> badges = new ArrayList<>();
        badges.add(new Badge("badge_1"));
        badges.add(new Badge("badge_2"));
        badges.add(new Badge("badge_3"));
        badges.add(new Badge("badge_3"));
        badges.add(new Badge("badge_3"));
        badges.add(new Badge("badge_3"));
        badges.add(new Badge("badge_3"));
        badges.add(new Badge("badge_4"));
        badges.add(new Badge("badge_5"));
        return badges;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
