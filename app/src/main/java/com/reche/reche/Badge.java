package com.reche.reche;

/**
 * Created by Mallock on 3/27/2018.
 */

public class Badge {
    private String badge_file_name;
    private String badge_name;

    public Badge(String badge_file_name) {
        this.badge_file_name = badge_file_name;
        this.badge_name = badge_file_name;
    }

    public String getBadge_file_name() {
        return badge_file_name;
    }

    public void setBadge_file_name(String badge_file_name) {
        this.badge_file_name = badge_file_name;
    }

    public String getBadge_name() {
        return badge_name;
    }

    public void setBadge_name(String badge_name) {
        this.badge_name = badge_name;
    }
}
