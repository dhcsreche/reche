package com.reche.reche;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ResearcherInterestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ResearcherInterestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResearcherInterestFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String TAG = this.getClass().getSimpleName();
    int MAX_CHARACTERS = 10;

    private OnFragmentInteractionListener mListener;

    public ResearcherInterestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ResearcherInterestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ResearcherInterestFragment newInstance(String param1, String param2) {
        ResearcherInterestFragment fragment = new ResearcherInterestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_researcher_interest, container, false);
        String tags_list[] = {"DHCS", "People", "Research", "FGD", "Reche"};
        ArrayList<String> tags = new ArrayList<>();
        tags.addAll(Arrays.asList(tags_list));

//        Collections.sort(tags, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.length() - o2.length();
//            }
//        });

        Log.e(TAG, String.valueOf(tags));
        final LinearLayout tagsParentLayout = view.findViewById(R.id.tags_parent_layout);

        int remaining = MAX_CHARACTERS;
        ArrayList<LinearLayout> linearLayouts = new ArrayList<>();
        LinearLayout currentLayout = new LinearLayout(getContext());
        currentLayout.setOrientation(LinearLayout.HORIZONTAL);

        int count = 0;
        for (String tag : tags) {
            Button button = new Button(getContext());
            button.setText(tag);
            button.setBackgroundResource(R.layout.oval_button);
            button.setPadding(5, 2, 5, 2);
            LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, tag.length());
            buttonLayoutParams.setMargins(50, 10, 0, 0);
            button.setLayoutParams(buttonLayoutParams);
            int buttonWidth = tag.length();
            if (buttonWidth <= remaining) {
                currentLayout.addView(button);
                remaining -= buttonWidth;
                Log.e(TAG, "added " + tag + " to " + count);
            } else {
                linearLayouts.add(currentLayout);
                count++;
                currentLayout = new LinearLayout(getContext());
                currentLayout.setOrientation(LinearLayout.HORIZONTAL);
                currentLayout.addView(button);
                remaining = MAX_CHARACTERS - buttonWidth;
                Log.e(TAG, "added " + tag + " to " + count);
            }
        }
        linearLayouts.add(currentLayout);
        for (LinearLayout linearLayout : linearLayouts) {
            tagsParentLayout.addView(linearLayout);
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
