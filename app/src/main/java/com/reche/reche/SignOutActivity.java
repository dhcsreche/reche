package com.reche.reche;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.reche.reche.SignInActivity.mGoogleSignInClient;

public class SignOutActivity extends AppCompatActivity {

    static public Boolean isRegistered = false;

    private String uname, uID, uemail, url;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    final DatabaseReference profilesRef = ref.child("profiles").push();
    String profileId;
    TextView aff;
    Switch isResearcher;
    EditText bio_edit;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_out);

        getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.lightBlueB));

        profileId = profilesRef.getKey();
        aff = (TextView)findViewById(R.id.affiliation_textview);
        isResearcher = (Switch) findViewById(R.id.is_researcher_button);

        bio_edit = findViewById(R.id.interests_textview);

        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();

        uname = intent.getStringExtra("user_name");
        uID = intent.getStringExtra("userID");
        uemail = intent.getStringExtra("user_email");
        if(extras.containsKey("profile_pic")) url = intent.getStringExtra("profile_pic");
        else url = null;

        if(isRegistered) {
            openFeedPage();
        } else {
            TextView usernameView = findViewById(R.id.userName);
            usernameView.setText(uname);

            if (url != null) {
                ImageView profilePic = findViewById(R.id.profile_pic);
                Picasso.get().load(url).into(profilePic);
            }
        }
    }

    @Override
    public void onBackPressed() {
        signOutFunc();
        Toast.makeText(SignOutActivity.this, "Signed Out", Toast.LENGTH_SHORT).show();
        super.onBackPressed();
    }

    public void signOutFunc() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        isRegistered = false;
                        // [START_EXCLUDE]
                        // [END_EXCLUDE]
                    }
                });
    }


    public void submitButton(View view) {
        String bio = bio_edit.getText().toString();
        List<String> interests = new ArrayList<>();
        interests.add("Computer Science");
        List<String> badges = new ArrayList<>();
        List<String> bookmarks = new ArrayList<>();
        List<String> ongoingTasks = new ArrayList<>();
        List<String> participatingTasks = new ArrayList<>();
        profilesRef.setValue(new Profile(uemail, uname, aff.getText().toString(), 5.0, true, badges, bio, interests, isResearcher.isChecked(), bookmarks, ongoingTasks, 100, participatingTasks));

        Intent intent = new Intent(this, FeedActivity.class);

        intent.putExtra("user_name", uname);
        intent.putExtra("user_email", uemail);
        intent.putExtra("authorID", profilesRef.getKey());
        StaticClass.userIdReche = profilesRef.getKey();
        if(url != null) intent.putExtra("profile_pic", url);

        isRegistered = true;

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    private void openFeedPage() {
        Intent intent = new Intent(this, FeedActivity.class);

        intent.putExtra("user_name", uname);
        intent.putExtra("user_email", uemail);
        intent.putExtra("authorID", profilesRef.getKey());
        StaticClass.userIdReche = profilesRef.getKey();
        if(url != null) intent.putExtra("profile_pic", url);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

}
