package com.reche.reche;

import android.util.Log;
import android.view.View;

/**
 * Created by nitinkumar on 10/04/18.
 */

public class itemClickListener implements View.OnClickListener{

    @Override
    public void onClick(View view) {
        int itemPosition = FeedActivity.recyclerView.getChildLayoutPosition(view);
        Event e = FeedActivity.eventData.get(itemPosition);
        Log.d("Adapter Activity", "On click listener is attached");
        if (view.getId() == R.id.bookmarkIcon) {
            if (e.getBookmarked() == false) {
                Log.d("Adapter Activity", "clicked on bookmark");
                FeedActivity.bookmarkList.add(e.getId());
                //Picasso.get().load(R.drawable.bookmark).into(view.getId());
            }
            else {
                Log.d("Adapter Activity", "clicked on list item");
                FeedActivity.bookmarkList.remove(e.getId());
                //Picasso.get().load(R.drawable.bookmarked).into(holder.bookmarkIcon);
            }
        }
        else {
            StaticClass.akshatString = FeedActivity.eventData.get(itemPosition).getId();
            //FeedActivity.startActivity(new Intent(mcontext, TaskDetailActivity.class));
        }
    }
}
