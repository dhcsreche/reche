package com.reche.reche;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ldoublem.loadingviewlib.view.LVNews;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.reche.reche.SignInActivity.mGoogleSignInClient;

public class FeedActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MyActivity";
    private EventItemAdapter adapter;
    static List<Event> eventData;
    static RecyclerView recyclerView;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference ref = database.getReference();
    DatabaseReference bookmarksRef;
    DatabaseReference participatingTasksRef;
    static List<String> bookmarkList;
    static List<String> participatingTasks;
    String hardcodedString = "-L9xXG8ArnM58W2B9Rhb";
    public final Context FeedContext = FeedActivity.this;
    private float y_loc;
    DatabaseReference userProfileRef;
    final DatabaseReference eventsRef = ref.child("events");
    boolean isFabUp = true;
    static String localUserId;

    Dialog dialog;
    LVNews block;
    DrawerLayout drawer;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_activity);

        getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.lightBlueB));

        showLoadingPopup();

        //localUserId = getIntent().getStringExtra("authorID");
        localUserId = StaticClass.userIdReche;

        //bookmarksRef = ref.child("profiles/"+ hardcodedString + "/bookmarks");
        //bookmarksRef = ref.child("profiles/" + getIntent().getStringExtra("authorID") + "/bookmarks");
        bookmarksRef = ref.child("profiles/" + StaticClass.userIdReche + "/bookmarks");

        //userProfileRef = ref.child("profiles/"+ hardcodedString);
        //userProfileRef = ref.child("profiles/" + getIntent().getStringExtra("authorID"));
        userProfileRef = ref.child("profiles/" + StaticClass.userIdReche);

        //participatingTasksRef = ref.child("profiles/"+ hardcodedString + "/bookmarks");
        //participatingTasksRef = ref.child("profiles/" + getIntent().getStringExtra("authorID") + "/participatingTasks");
        participatingTasksRef = ref.child("profiles/" + StaticClass.userIdReche + "/participatingTasks");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        FirebaseApp.initializeApp(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();

                Intent intentx = new Intent(FeedActivity.this, TaskCreation.class);

                //intentx.putExtra("user_name", "Nitin Kumar");
                //intentx.putExtra("user_name", getIntent().getStringExtra("user_name"));
                intentx.putExtra("user_name", StaticClass.userName);

                //intentx.putExtra("authorID", hardcodedString);
                //intentx.putExtra("authorID", getIntent().getStringExtra("authorID"));
                intentx.putExtra("authorID", StaticClass.userIdReche);

                startActivity(intentx);

                //startActivity(new Intent(FeedActivity.this, TaskCreation.class));

            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initialiseText();
        makeFabHiddenOnScroll(fab, recyclerView);
    }

    private void showLoadingPopup() {
        dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_loading);
        dialog.show();
        block = dialog.findViewById(R.id.loading_block);
        block.setViewColor(0x0088CC);
        block.startAnim(1000);
    }

    private void makeFabHiddenOnScroll(final FloatingActionButton floatingActionButton, RecyclerView recyclerView) {


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int[] location = new int[2];
                floatingActionButton.getLocationOnScreen(location);
                if (y_loc == 0 && dy != 0) {
                    y_loc = location[1] - 81;
                }

                if (dy > 0 && isFabUp) {
                    ViewPropertyAnimator animate = floatingActionButton.animate();
                    animate.setDuration(200)
                            .yBy(300)
                            .start();
                    Log.e(TAG, "a");
                    isFabUp = false;

                } else if (dy < 0 && !isFabUp) {
                    ViewPropertyAnimator animate = floatingActionButton.animate();
                    animate.setDuration(200)
                            .yBy(-300)
                            .start();
                    Log.e(TAG, "b");
                    isFabUp = true;
                }
            }
        });
    }

    @Override
    //Again check whether this needs to be in onStart..ie refresh functionality
    protected void onStart() {

        super.onStart();
        //setContentView(R.layout.activity_main);
        eventData = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        userProfileRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                participatingTasks = dataSnapshot.getValue(Profile.class).getParticipatingTasks();
                if (participatingTasks == null) {
                    Log.d("FB", "Null participating list");
                    participatingTasks = new ArrayList<String>();
                }
                bookmarkList = dataSnapshot.getValue(Profile.class).getBookmarks();
                if (bookmarkList == null) {
                    Log.d("FB", "Null bookmark");
                    bookmarkList = new ArrayList<String>();
                }
                eventsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                for (DataSnapshot eventItem : dataSnapshot.getChildren()) {
                                    Event newEvent = eventItem.getValue(Event.class);
                                    eventData.add(newEvent);
                                    //Log.d(TAG+"helooooooooo", String.valueOf(eventItem));
                                }
                                ArrayList<Event> tempRemove = new ArrayList<>();
                                for (Event e : eventData) {
                                    if (getSupportActionBar().getTitle().equals("Participating Tasks")) {
                                        if (participatingTasks.contains(e.getId()) == false) {
                                            tempRemove.add(e);
                                        }
                                    } else if (getSupportActionBar().getTitle().equals("Active Feed")) {
                                        if (participatingTasks.contains(e.getId())) {
                                            tempRemove.add(e);
                                        }
                                    }

                                    if (bookmarkList.contains(e.getId())) {
                                        e.setBookmarked(true);
                                    } else {
                                        e.setBookmarked(false);
                                        if (getSupportActionBar().getTitle().equals("Bookmarks")) {
                                            tempRemove.add(e);
                                        }
                                    }
                                }
                                eventData.removeAll(tempRemove);
                                Collections.reverse(eventData);
                                adapter = new EventItemAdapter(FeedActivity.this, eventData, userProfileRef);

                                recyclerView.setHasFixedSize(true);
                                recyclerView.setItemViewCacheSize(50);
                                recyclerView.setDrawingCacheEnabled(true);
                                recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                recyclerView.setAdapter(adapter);

                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                //final ImageView bookmarkButton = (ImageView) findViewById(R.id.bookmarkIcon);
                                dialog.dismiss();
                            }
                        }, 2000);
                        Log.d(TAG, String.valueOf(dataSnapshot));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                //Log.d(TAG, String.valueOf(eventData.get(0).getTags()));

                /*
                recyclerView.addOnItemTouchListener(
                        new RecyclerItemClickListener(FeedActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                //StaticClass.akshatString = eventData.get(position).getId();
                                //startActivity(new Intent(FeedActivity.this, TaskDetailActivity.class));
                                //Toast.makeText(FeedActivity.this, position + "'", Toast.LENGTH_LONG).show();
                                //Toast.makeText(FeedActivity.this, eventData.get(position).getId(), Toast.LENGTH_LONG).show();
                                Event e = eventData.get(position);
                                Log.d("Adapter Activity", "On click listener is attached");
                                if (view.getId() == R.id.bookmarkIcon) {
                                    if (e.getBookmarked() == false) {
                                        Log.d("Adapter Activity", "clicked on bookmark");
                                        FeedActivity.bookmarkList.add(e.getId());
                                        //Picasso.get().load(R.drawable.bookmark).into(holder.bookmarkIcon);
                                    }
                                    else {
                                        Log.d("Adapter Activity", "clicked on list item");
                                        FeedActivity.bookmarkList.remove(e.getId());
                                        //Picasso.get().load(R.drawable.bookmarked).into(holder.bookmarkIcon);
                                    }
                                }
                                else {
                                    StaticClass.akshatString = eventData.get(position).getId();
                                    startActivity(new Intent(FeedActivity.this, TaskDetailActivity.class));
                                }

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                // do whatever
                            }
                        })
                );*/


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        bookmarksRef.setValue(bookmarkList);
                        eventData.clear();
                        eventsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot eventItem : dataSnapshot.getChildren()) {
                                    Event newEvent = eventItem.getValue(Event.class);
                                    eventData.add(newEvent);
                                }
                                ArrayList<Event> tempRemove = new ArrayList<>();
                                for (Event e : eventData) {
                                    if (getSupportActionBar().getTitle().equals("Participating Tasks")) {
                                        if (participatingTasks.contains(e.getId()) == false) {
                                            tempRemove.add(e);
                                        }
                                    } else if (getSupportActionBar().getTitle().equals("Active Feed")) {
                                        if (participatingTasks.contains(e.getId())) {
                                            tempRemove.add(e);
                                        }
                                    }
                                    if (bookmarkList.contains(e.getId())) {
                                        e.setBookmarked(true);
                                    } else {
                                        e.setBookmarked(false);
                                        if (getSupportActionBar().getTitle().equals("Bookmarks")) {
                                            tempRemove.add(e);
                                        }
                                    }
                                }
                                eventData.removeAll(tempRemove);
                                Collections.reverse(eventData);
                                adapter.notifyDataSetChanged();
                                //Log.d(TAG, String.valueOf(eventData.get(0).getTags()));

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        mySwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("OnPause", "OnPause");
        bookmarksRef.setValue(bookmarkList);
        //participatingTasksRef.setValue(participatingTasks);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            this.finishAffinity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.feed_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.sign_out_button) {
            signOutFunc();
        }
        if (id == R.id.participating_tasks) {
            getSupportActionBar().setTitle("Participating Tasks");
            eventData.clear();
            eventsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot eventItem : dataSnapshot.getChildren()) {
                        Event newEvent = eventItem.getValue(Event.class);
                        eventData.add(newEvent);
                    }
                    ArrayList<Event> tempRemove = new ArrayList<>();
                    for (Event eve : eventData) {
                        if (bookmarkList.contains(eve.getId())) {
                            eve.setBookmarked(true);
                        }
                        if (participatingTasks.contains(eve.getId()) == false) {
                            tempRemove.add(eve);
                        }
                    }
                    eventData.removeAll(tempRemove);
                    //Log.d(TAG, String.valueOf(eventData.get(0).getTags()));
                    Collections.reverse(eventData);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        if (id == R.id.bookmarks_button) {
            getSupportActionBar().setTitle("Bookmarks");
            bookmarksRef.setValue(bookmarkList);
            eventData.clear();
            eventsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot eventItem : dataSnapshot.getChildren()) {
                        Event newEvent = eventItem.getValue(Event.class);
                        eventData.add(newEvent);
                    }
                    ArrayList<Event> tempRemove = new ArrayList<>();
                    for (Event eve : eventData) {
                        if (bookmarkList.contains(eve.getId()) == false) {
                            tempRemove.add(eve);
                        } else {
                            eve.setBookmarked(true);
                        }
                        /*
                        if (participatingTasks.contains(eve.getId())) {
                            tempRemove.add(eve);
                        }
                        */
                    }
                    eventData.removeAll(tempRemove);
                    //Log.d(TAG, String.valueOf(eventData.get(0).getTags()));
                    Collections.reverse(eventData);
                    adapter.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        if (id == R.id.nav_camera) {
            getSupportActionBar().setTitle("Active Feed");
            bookmarksRef.setValue(bookmarkList);
            eventData.clear();
            eventsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot eventItem : dataSnapshot.getChildren()) {
                        Event newEvent = eventItem.getValue(Event.class);
                        eventData.add(newEvent);
                    }
                    ArrayList<Event> tempRemove = new ArrayList<>();
                    for (Event e : eventData) {
                        if (participatingTasks.contains(e.getId())) {
                            tempRemove.add(e);
                        }
                        if (bookmarkList.contains(e.getId())) {
                            e.setBookmarked(true);
                        } else {
                            e.setBookmarked(false);
                        }
                    }
                    eventData.removeAll(tempRemove);
                    //Log.d(TAG, String.valueOf(eventData.get(0).getTags()));
                    Collections.reverse(eventData);
                    adapter.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initialiseText() {

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        Intent intent = getIntent();

        //String jugaad = "Jugaad";
        TextView name = (TextView) header.findViewById(R.id.user_name_feed);
        //name.setText(jugaad);
        //name.setText(intent.getStringExtra("user_name"));
        name.setText(StaticClass.userName);

        TextView email = (TextView) header.findViewById(R.id.user_email_feed);
        //email.setText(jugaad);
        //email.setText(intent.getStringExtra("user_email"));
        email.setText(StaticClass.userEmailId);

        ImageView profilePic = (ImageView) header.findViewById(R.id.dp);
        String url = StaticClass.userProfilePic;
        if (url != null) {
            Picasso.get().load(url).into(profilePic);
        }
        else {
            Picasso.get().load(R.drawable.man).into(profilePic);
        }


        /*
        if (intent.getExtras().containsKey("profile_pic")) {
            ImageView profilePic = (ImageView) header.findViewById(R.id.dp);
            String url = intent.getStringExtra("profile_pic");
            Picasso.get().load(url).into(profilePic);
        }
        */


    }

    public void signOutFunc(View view) {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        Toast.makeText(FeedActivity.this, "Signed Out", Toast.LENGTH_SHORT).show();
                        SignOutActivity.isRegistered = false;
                        updateUI();
                        // [END_EXCLUDE]
                    }
                });
    }

    private void signOutFunc() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        Toast.makeText(FeedActivity.this, "Signed Out", Toast.LENGTH_SHORT).show();
                        SignOutActivity.isRegistered = false;
                        updateUI();
                        // [END_EXCLUDE]
                    }
                });
    }


    private void updateUI() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();
    }
}
