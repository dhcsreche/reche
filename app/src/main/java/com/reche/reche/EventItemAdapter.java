package com.reche.reche;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by nitinkumar on 10/03/18.
 */

public class EventItemAdapter extends RecyclerView.Adapter<EventItemAdapter.CardViewHolder>{

    public Context mcontext;
    public List<Event> eventData;
    public DatabaseReference userProfileRef;
    //public View.OnClickListener clickListener;

    public EventItemAdapter(Context mcontext, List<Event> eventData, DatabaseReference userProfileRef) {
        this.mcontext = mcontext;
        this.eventData = eventData;
        this.userProfileRef = userProfileRef;
    }

    public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView authorName, description, incentiveValue, timeValue, typeValue;
        public ImageView incentiveType, typeSymbol, bookmarkIcon, timeSymbol;
        public Button hashtag1, hashtag2;


        public CardViewHolder(View itemView) {
            super(itemView);
            authorName = (TextView) itemView.findViewById(R.id.authorName);
            description = (TextView) itemView.findViewById(R.id.description);
            incentiveValue = (TextView) itemView.findViewById(R.id.incentiveValue);
            timeValue = (TextView) itemView.findViewById(R.id.timeValue);
            typeValue = (TextView) itemView.findViewById(R.id.typeValue);

            incentiveType = (ImageView) itemView.findViewById(R.id.incentiveType);
            typeSymbol = (ImageView) itemView.findViewById(R.id.typeSymbol);
            bookmarkIcon = (ImageView) itemView.findViewById(R.id.bookmarkIcon);
            timeSymbol = (ImageView) itemView.findViewById(R.id.timeSymbol);

            hashtag1 = (Button) itemView.findViewById(R.id.hashtag1);
            hashtag2 = (Button) itemView.findViewById(R.id.hashtag2);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = FeedActivity.recyclerView.getChildLayoutPosition(view);
            Log.d("Adapter Activity", itemPosition + "");
            Event e = FeedActivity.eventData.get(itemPosition);
            Log.d("Adapter Activity", "On click listener is attached");
            if (view.getId() == bookmarkIcon.getId()) {
                if (e.getBookmarked() == false) {
                    Log.d("Adapter Activity", "clicked on bookmark");
                    FeedActivity.bookmarkList.add(e.getId());
                    Picasso.get().load(R.drawable.bookmark).into(bookmarkIcon);
                }
                else {
                    Log.d("Adapter Activity", "clicked on list item");
                    FeedActivity.bookmarkList.remove(e.getId());
                    Picasso.get().load(R.drawable.bookmarked).into(bookmarkIcon);
                }
            }
            else {
                StaticClass.akshatString = eventData.get(itemPosition).getId();
                mcontext.startActivity(new Intent(mcontext, TaskDetailActivity.class));
            }
        }
    }

    @Override
    public EventItemAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View eventCard = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item_new_new, parent, false);
        //eventCard.setOnClickListener(clickListener);

        return new CardViewHolder(eventCard);
    }

    @Override
    public void onBindViewHolder(final EventItemAdapter.CardViewHolder holder, final int position) {
        final Event e = eventData.get(position);
        holder.authorName.setText(e.getAuthorName());
        holder.description.setText(e.getDescription());
        holder.incentiveValue.setText(e.getIncentiveValue()+"");
        holder.timeValue.setText(e.getTime());
        holder.typeValue.setText(e.getType());

        holder.hashtag1.setText(e.getTags().get(0));
        holder.hashtag2.setText(e.getTags().get(1));
        if (e.getIncentiveType().equals("Amazon")) {
//            Picasso.with(this.mcontext).load(R.drawable.amazon).into(holder.incentiveType);
            Picasso.get().load(R.drawable.amazon).into(holder.incentiveType);
        }
        if (e.getIncentiveType().equals("Paytm")) {
            Picasso.get().load(R.drawable.paytm).into(holder.incentiveType);
        }
        if (e.getIncentiveType().equals("Cash")) {
            Picasso.get().load(R.drawable.rupee).into(holder.incentiveType);
        }
        if (e.getType().equals("FGD")) {
            Picasso.get().load(R.drawable.fgd).into(holder.typeSymbol);
        }
        if (e.getType().equals("Survey")) {
            Picasso.get().load(R.drawable.survey).into(holder.typeSymbol);
        }
        if (e.getType().equals("Interview")) {
            Picasso.get().load(R.drawable.interview).into(holder.typeSymbol);
        }

        if (e.getBookmarked() == false) {
            Picasso.get().load(R.drawable.bookmark).into(holder.bookmarkIcon);
        }
        else {
            Picasso.get().load(R.drawable.bookmarked).into(holder.bookmarkIcon);
        }

        holder.bookmarkIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d("Feed Activity", "Bookmark: " + e.getBookmarked() + "");
                //Event e = FeedActivity.eventData.get(itemPosition);
                if (e.getBookmarked() == false) {
                    Log.d("Feed Activity", "clicked on bookmark");
                    FeedActivity.bookmarkList.add(e.getId());
                    Picasso.get().load(R.drawable.bookmarked).into(holder.bookmarkIcon);
                    e.setBookmarked(true);
                }
                else {
                    Log.d("Feed Activity", "clicked on list item");
                    FeedActivity.bookmarkList.remove(e.getId());
                    Picasso.get().load(R.drawable.bookmark).into(holder.bookmarkIcon);
                    e.setBookmarked(false);
                }
            }
        });


        /*
        clickListener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.d("Adapter Activity", "On click listener is attached");
                if (view.getId() == R.id.bookmarkIcon) {
                    if (e.getBookmarked() == false) {
                        Log.d("Adapter Activity", "clicked on bookmark");
                        FeedActivity.bookmarkList.add(e.getId());
                        Picasso.get().load(R.drawable.bookmark).into(holder.bookmarkIcon);
                    }
                    else {
                        Log.d("Adapter Activity", "clicked on list item");
                        FeedActivity.bookmarkList.remove(e.getId());
                        Picasso.get().load(R.drawable.bookmarked).into(holder.bookmarkIcon);
                    }
                }
                else {
                    StaticClass.akshatString = eventData.get(position).getId();
                    mcontext.startActivity(new Intent(mcontext, TaskDetailActivity.class));
                }
            }
        };*/

    }

    @Override
    public int getItemCount() {
        return eventData.size();
    }
}
