package com.reche.reche;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class AddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        //need to make this static
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        DatabaseReference eventsRef = ref.child("events");
        DatabaseReference newEventRef = eventsRef.push();
        String eventId = newEventRef.getKey();
        List<String> tags = new ArrayList<>();
        List<String> reportedBy = new ArrayList<>();
        tags.add("#AI");
        tags.add("#ImageAnalysis");
        reportedBy.add("a1");
        reportedBy.add("a2");
        newEventRef.setValue(new Event(eventId, tags, "a1", "Dr PK", "Test",
                "Urgently need participants for focused group discussion on 10th march", reportedBy, "FGD", "10 Hrs Long", "Amazon", 100));
        Toast.makeText(this,eventId, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        //intent.putExtra("EXTRA_SESSION_ID", sessionId);
        startActivity(intent);

    }
}
